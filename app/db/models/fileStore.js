const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const fileStoreSchema = new Schema({
    fileId: {
        type: String,
        required : true
    },
    preview: {
        type: String,
        required : true
    },
    fileUrl: {
        type: String,
        required : true
    }
});

const FileStore = mongoose.model("FileStore", fileStoreSchema);
module.exports = FileStore;

