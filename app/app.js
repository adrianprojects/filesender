const express = require("express");

const app = new express();

//init Database
require("./db/mongoose.js");

app.use(express.static("public"));

app.use(express.urlencoded( { extended: true }));
app.use(express.json());

app.use("/api/", require("./routes/api.js"));

module.exports = app;