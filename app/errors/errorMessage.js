module.exports = {

    badExtension: "Nieobsługiwane rozszerzenie pliku.",
    noFile: "Brak pliku",
    noPreviewInBase: "Nie znaleziono preview dla podanego identyfikatora pliku."

}