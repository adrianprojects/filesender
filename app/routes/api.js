const express = require("express");
const router = new express.Router();
const upload = require("../services/uploader");
const fileStoreController = require("../controllers/fileStoreController");

router.get("/getPreview/:fileId", fileStoreController.getPreviewByFileId);
router.post("/fileSender", upload.single("file"), fileStoreController.saveFileAndPreview);

module.exports = router;