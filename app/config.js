require("dotenv").config();

module.exports = {
    port: process.env.PORT || 3000,
    database: process.env.DATABASE || "mongodb://root:password@localhost:27017/databaseName?authSource=admin",
    awskey: process.env.AWS_KEY,
    awsSecret: process.env.AWS_SECRET,
    awsEndpoint: process.env.AWS_ENDPOINT,
    awsRegion: process.env.AWS_REGION,
    awsBucket: process.env.AWS_BUCKET,
    previewSize: 6,

}