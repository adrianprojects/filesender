const FileStore = require("../db/models/fileStore");
const readline = require("readline");
const { previewSize } = require("../config");
const { Duplex } = require("stream");

const savePreview = async (file, fileId, filePath) => {

    try {

        const fileStream = bufferToStream(file.buffer);
        const rl = readline.createInterface({
            input: fileStream,
            crlfDelay: Infinity
        });

        const lines = []
        let counter = 0;
        for await (const line of rl) {
            if(counter === previewSize) {
                break;
            }

            lines.push(line);
            counter++;
        }

        const preview = JSON.stringify(lines);
        const fileStore = new FileStore({
            fileId,
            preview,
            fileUrl: filePath
        });

        await fileStore.save();

    } catch (e) {
        console.log(e.errors);
    }
}

function bufferToStream(buffer) {

    let tmp = new Duplex();
    tmp.push(buffer);
    tmp.push(null);
    return tmp;
}

module.exports = savePreview;