const AWS = require("aws-sdk");
const { awskey, awsSecret, awsBucket, awsEndpoint, awsRegion } = require("../config");

const s3 = new AWS.S3({
    accessKeyId: awskey,
    secretAccessKey: awsSecret,
    endpoint: awsEndpoint,
    region: awsRegion,
    s3ForcePathStyle: true
});

module.exports = {

    async saveAwsFile(file, uuid) {

        try{

            const params = {
                Bucket: awsBucket,
                Key: `uploads/${uuid}-${file.originalname}`,
                ContentType: file.mimetype,
                Body: file.buffer,
                ACL: "public-read"
            }
    
            return await s3.upload(params).promise();

        } catch(e){
            console.log(e);
        }
        
    }
}

