const supportedExtension = require("../supportedExtension");

module.exports = {

    fileExtensionValidation(fileExtension) {

        const allowExtensions = [supportedExtension.csv];
        return allowExtensions.includes(fileExtension);
    }
}