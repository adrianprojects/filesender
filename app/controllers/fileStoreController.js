const { v4: uuidv4 } = require("uuid");
const savePreview = require("../services/savePreview");
const validations = require("../validations/validations");
const errorMessage = require("../errors/errorMessage");
const uploadFileAws = require("../services/awsUploader");
const path = require("path");
const supportedExtension = require("../supportedExtension");
const FileStore = require("../db/models/fileStore");

class FileStoreController {

    async saveFileAndPreview(req, res) {

        if(!req.file) {
            return res.status(404).json({ error: errorMessage.noFile })
        }

        const fileExtension = path.extname(req.file.originalname);

        if(!validations.fileExtensionValidation(fileExtension)) {
            return res.status(415).json({ error: errorMessage.badExtension });
        }

        const uuid = uuidv4();

        const backGroundSaveProcess = async () => {
            const response = await uploadFileAws.saveAwsFile(req.file, uuid);

            test.push("jakaś wartość testowa");
            if(response?.Location) {
                
                switch(fileExtension) {
                    case supportedExtension.csv:
                        savePreview(req.file, uuid, response.Location);
                        break;
                }
            }  
        }
        
        backGroundSaveProcess();
        
        res.json({ fileId: uuid });
    }

    async getPreviewByFileId(req, res) {

        const { fileId } = req.params;

        const fileStore = await FileStore.findOne( { fileId: fileId });

        if(!fileStore?.preview) {
            return res.status(404).json( { error: errorMessage.noPreviewInBase });
        }

        res.json({ preview: JSON.parse(fileStore.preview) });
    }
}

module.exports = new FileStoreController();